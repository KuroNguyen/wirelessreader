#include "w25qxx.h"
#include <SPI.h>
#define W25QXX_DUMMY_BYTE   0xA5

//w25qxx_t w25qxx;

// SPIClass * _spi = NULL;

//######################################### Constructor ############################################//
W25qxx::W25qxx(SPIClass *spi, uint8_t csPin) {
    _spi = spi;
    _csPin = csPin;
    pinMode(_csPin,OUTPUT);
}

bool W25qxx::begin(uint32_t flashChipSize) {
    return true;
}

//####################################################################//
//                         Public functions                           //
//####################################################################//

uint32_t W25qxx::W25qxx_ReadID(void) {
    _getJedecId();
    _isChipPoweredDown();
    _enable4ByteAddressing();
    
    // if (!_notBusy()) {
    //     Serial.println("Error");
    //     return 0;
    // }
    // uint32_t Temp = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;
    // // HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_RESET);
    // CHIP_DESELECT
    // CHIP_SELECT

    // W25qxx_Spi(JEDECID);
    // Temp0 = W25qxx_Spi(W25QXX_DUMMY_BYTE);
    // Temp1 = W25qxx_Spi(W25QXX_DUMMY_BYTE);
    // Temp2 = W25qxx_Spi(W25QXX_DUMMY_BYTE);
    // // HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_SET);
    // digitalWrite(_csPin,0);
    // Temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;
    
    return _chip.manufacturerID;
    
}

/**
 * Writes a byte of data to a specific location in a page
 * Takes three arguments - 
 *     1. _addr --> Any address - from 0 to capacity
 *     2. data --> One byte to be written to a particular location on a page
 *     3. errorCheck --> Turned on by default. Checks for writing errors
 * WARNING: You can only write to previously erased memory locations (see datasheet).
 * Use the eraseSector()/eraseBlock32K/eraseBlock64K commands to first clear memory (write 0xFFs)
 */
bool W25qxx::writeByte(uint32_t _addr, uint8_t data, bool errorCheck) {
    // Debug
    Serial.printf("W25qxx WriteByte 0x%02X at address %d begin...", data, _addr);

    uint32_t time = micros();
    _notBusy();
    _writeEnable();
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(PAGEPROG);
    // 3 byte address MSB
    W25qxx_Spi((_addr & 0xFF0000) >> 16);
    W25qxx_Spi((_addr & 0xFF00) >> 8);
    W25qxx_Spi(_addr & 0xFF);
    W25qxx_Spi(data);
    CHIP_DESELECT
    _notBusy();
    Serial.printf("w25qxx WriteByte done after %d us\r\n",micros()-time);
}

/**
 * Writes an array of bytes starting from a specific location in a page.
 * Takes 4 arguments -
 *      1. _addr --> Any address - from 0 to capacity
 *      2. data_buffer --> The pointer to the array of bytes be written to a particular location on a page
 *      3. bufferSize --> Size of the array of bytes - in number of bytes
 *      4. errorCheck --> Turned on by default. Checks for writing errors
 * WARNING: You can only write to previously erased memory locations (see datasheet).
 * Use the eraseSector()/eraseBlock32K/eraseBlock64K commands to first clear memory (write 0xFFs)
 */
bool W25qxx::writeByteArray(uint32_t _addr, uint8_t *data_buffer, size_t bufferSize, bool errorCheck) {
    // Debug
    Serial.printf("w25qxx WriteByteArray at address %d\r\n",_addr);
    uint32_t time = micros();

    uint16_t maxBytes = SPI_PAGESIZE - (_addr % SPI_PAGESIZE);  // Force the first set of bytes to stay within the first page
    uint32_t currentAddr = _addr;

    if (bufferSize <= maxBytes) {
        Serial.println("Easy stuff");

        _notBusy();
        _writeEnable();
        CHIP_DESELECT
        CHIP_SELECT
        // if (!_notBusy() !! !_writeEnable()) {
        //     return false;
        // }
        W25qxx_Spi(PAGEPROG);
        // 3 byte address MSB
        W25qxx_Spi((_addr & 0xFF0000) >> 16);
        W25qxx_Spi((_addr & 0xFF00) >> 8);
        W25qxx_Spi(_addr & 0xFF);
        for (uint16_t i = 0; i < bufferSize; i++) {
            W25qxx_Spi(data_buffer[i]);
            // Serial.printf("data write 0x%02X\r\n",data_buffer[i]);
        }
        CHIP_DESELECT
        _notBusy();
    } else {
        uint16_t length = bufferSize;
        uint16_t writeBufSz;
        uint16_t data_offset = 0;
        uint8_t count = 0;
        // Write first page
        do {
            Serial.printf("Round %d\r\n",count);
            count++;
            Serial.printf("Current address %d\r\n",currentAddr);

            writeBufSz = (length<=maxBytes) ? length : maxBytes;    

            

            _notBusy();
            _writeEnable();
            CHIP_DESELECT
            CHIP_SELECT
    
            // _notBusy();
            // _writeEnable();
            W25qxx_Spi(PAGEPROG);
            // 3 byte address MSB
            W25qxx_Spi((currentAddr & 0xFF0000) >> 16);
            W25qxx_Spi((currentAddr & 0xFF00) >> 8);
            W25qxx_Spi(currentAddr & 0xFF);
            for (uint16_t i = 0; i < writeBufSz; i++) {
                W25qxx_Spi(data_buffer[data_offset + i]);
                // Serial.printf("data write 0x%02X\r\n",data_buffer[i]);
            }
            
            CHIP_DESELECT
            currentAddr += writeBufSz;
            data_offset += writeBufSz;
            length -= writeBufSz;
            maxBytes = 256;

            if(!_notBusy() || !_writeEnable()){
                return false;
            }

        } while (length > 0);
        
    }
    Serial.printf("w25qxx WriteByteArray done after %d us\r\n",micros() - time);
    return true;
}

// Erases whole chip. Think twice before using.
bool W25qxx::eraseChip(void) {
    if(_isChipPoweredDown() || !_notBusy() || !_writeEnable()) {
        return false;
    }
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(CHIPERASE);
    CHIP_DESELECT
    _notBusy();
    return true;
}

/**
 * Reads a byte of data from a specific location in a page
 * Takes two arguments -
 *      1. _addr --> Any address from 0 to capacity
 *      2. fastRead --> defaults to false - executes _beginFastRead() if set to true
 */
uint8_t W25qxx::readByte(uint32_t _addr, bool fastRead) {
    // Debug
    Serial.printf("w25qxx ReadByte at address %d begin...\r\n", _addr);
    uint32_t time = micros();

    _notBusy();
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(READDATA);

    W25qxx_Spi((_addr & 0xFF0000) >> 16);
    W25qxx_Spi((_addr & 0xFF00) >> 8);
    W25qxx_Spi(_addr & 0xFF);
    // W25qxx_Spi(0);

    uint8_t data = W25qxx_Spi(0);
    CHIP_DESELECT

    Serial.printf("w25qxx ReadByte 0x%02X done after %d us\r\n",data,micros()-time);
    return data;
}

/**
 * Reads an array of byte from a specific location in a page
 * Takes 4 arguments -
 *      1. _addr --> Any address from 0 to capacity
 *      2. data_buffer --> The pointer to the array of bytes to be read
 *      3. bufferSize --> Size of the array of bytes - in number of bytes
 *      4. fastRead --> defaults to false - executes _beginFastRead() if set to true
 */
bool W25qxx::readByteArray(uint32_t _addr, uint8_t *data_buffer, size_t bufferSize, bool fastRead) {
    // Debug
    // Serial.printf("w25qxx ReadByteArray at address:%d, %d Bytes begin...\r\n", _addr, bufferSize);
    Serial.printf("w25qxx ReadByteArray at address:0x%08x, %d Bytes begin...\r\n", _addr, bufferSize);
    uint32_t time = micros();
    _notBusy();
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(READDATA);

    W25qxx_Spi((_addr & 0xFF0000) >> 16);
    W25qxx_Spi((_addr & 0xFF00) >> 8);
    W25qxx_Spi(_addr & 0xFF);
    // W25qxx_Spi(0);

    for (uint16_t i = 0; i < bufferSize; i++) {
        *data_buffer++ = W25qxx_Spi(0);
    }
    CHIP_DESELECT

    Serial.printf("w25qxx ReadByteArray done after %d us\r\n",micros()-time);
    return true;
}



//####################################################################//
//                        Private functions                           //
//####################################################################//




