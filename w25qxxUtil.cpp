#include "w25qxx.h"

// //####################################################################//
// //                              Macros                                //
// //####################################################################//
// #define _delay_us(us)   delayMicroseconds(us)
// #define CHIP_SELECT		digitalWrite(_csPin, LOW);
// #define CHIP_DESELECT	digitalWrite(_csPin, HIGH);


//####################################################################//
//                        Private functions                           //
//####################################################################//
uint8_t W25qxx::W25qxx_Spi(uint8_t Data) {
    uint8_t ret;
    ret = _spi->transfer(Data);
    return ret;
}

bool W25qxx::_notBusy(uint32_t timeout) {
    _delay_us(WINBOND_WRITE_DELAY);
    uint32_t _time = micros();
    do {
        uint8_t stat1 = _readStat1();
        if (!(stat1 & BUSY)) {
            return true;
        }
    } while ((micros() - _time) < timeout);
    if (timeout <= (micros() - _time)) {
        Serial.println("CHIP Busy");
        return false;
    }    
    return true;    
}

// Checks if status register 1 can be accessed - used to check chip status, during powerdown and power up and for debugging
uint8_t W25qxx::_readStat1(void) {
    CHIP_DESELECT
    CHIP_SELECT
    // Send instruction
    W25qxx_Spi(READSTAT1);
    // Get response byte
    stat1 = W25qxx_Spi(0);
    CHIP_DESELECT
    return stat1;
}

// Checks if status register 2 can be accessed, if yes, reads and returns it
uint8_t W25qxx::_readStat2(void) {
    // _beginSPI(READSTAT2);
    // stat2 = _nextByte(READ);
    //stat2 = _nextByte(READ);
    CHIP_DESELECT
    CHIP_SELECT
    // Send instruction
    W25qxx_Spi(READSTAT2);
    // Get response byte
    stat2 = W25qxx_Spi(0);
    CHIP_DESELECT
    return stat2;
}

// Checks if status register 3 can be accessed, if yes, reads and returns it
uint8_t W25qxx::_readStat3(void) {
    CHIP_DESELECT
    CHIP_SELECT
    // Send instruction
    W25qxx_Spi(READSTAT3);
    // Get response byte
    stat3 = W25qxx_Spi(0);
    CHIP_DESELECT
    return stat3;
}

//Enables writing to chip by setting the WRITEENABLE bit
bool W25qxx::_writeEnable(void) {
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(WRITEENABLE);
    CHIP_DESELECT
    if (!(_readStat1() & WRTEN)) {
        return false;
    }
    return true;
}

// Disable writing to chip by reset the WRITEENABLE bit
bool W25qxx::_writeDisable(void) {
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(WRITEDISABLE);
    CHIP_DESELECT
    return true;
}

// Checks for presence of chip by requesting JEDEC ID
bool W25qxx::_getJedecId(void) {
    if (!_notBusy()) {
        return false;
    }
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(JEDECID);
    _chip.manufacturerID = W25qxx_Spi(0); // manufacturer id
    _chip.memoryTypeID = W25qxx_Spi(0); // memory type
    _chip.capacityID = W25qxx_Spi(0); // capacity
    CHIP_DESELECT  
    if (!_chip.manufacturerID) {
        Serial.println("Get JedecID error");
        return false;
    }
    return true;
}

// Double checks all parameters before calling a read or write. Comes in two variants
// Takes address and returns the address if true, else returns false. Throws an error if there is a problem.
bool W25qxx::_prep(uint8_t opcode, uint32_t _addr, uint32_t size) {
    // If the flash memory is >= 256 MB enable 4-byte addressing
    if (_chip.manufacturerID == WINBOND_MANID && _addr >= MB(16)) {
        if (!_enable4ByteAddressing()) {    // If unable to enable 4-byte addressing 
            return false;
        }
    }
    switch (opcode) {
        case PAGEPROG:
            //Serial.print(F("Address being prepped: "));
            //Serial.println(_addr);
            if (_isChipPoweredDown());
            break;
        
        default:
            break;
    }
    return true;
}

// Checks to see if chip is powered down. If it is, returns true. If not, returns false.
bool W25qxx::_isChipPoweredDown(void) {
    if (chipPoweredDown) {
        Serial.println("Chip is powered down");
        return true;
    } else {
        return false;
    }
}

// Checks to see if 4-byte addressing is already enabled and if not, enables it
bool W25qxx::_enable4ByteAddressing(void) {
    Serial.println("Enable 4 address");
    Serial.println(_readStat3());
    if (_readStat3() & ADS) {
        return true;
    }
    CHIP_DESELECT
    CHIP_SELECT
    W25qxx_Spi(ADDR4BYTE_EN);
    CHIP_DESELECT
    delay(1);
    Serial.println(_readStat3());
    if (_readStat3() % ADS) {
        address4ByteEnabled = true;
        return true;
    } else {
        Serial.println("4 byte address error");
        return false;
    }
}



