#ifndef _W25QXX_H
#define _W25QXX_H

#include <stdbool.h>
#include <Arduino.h>
#include <SPI.h>

//####################################################################//
//                             Common Instructions                    //
//####################################################################//
#define MANID			0x90
#define PAGEPROG		0x02
#define READDATA		0x03
#define FASTREAD		0x0B
#define WRITEDISABLE	0x04
#define READSTAT1		0x05
#define READSTAT2 		0x35
#define READSTAT3		0x15
#define WRITESTATEN		0x50
#define WRITESTAT1 		0x01
#define WRITESTAT2		0x31
#define WRITESTAT3		0x11
#define WRITEENABLE		0x06
#define ADDR4BYTE_EN	0xB7
#define ADDR4BYTE_DIS	0xE9
#define SECTORERASE		0x20
#define BLOCK32ERASE	0x52
#define BLOCK64ERASE	0xD8
#define CHIPERASE		0x60
#define ALT_CHIPERASE	0xC7
#define SUSPEND			0x75
#define id				0x90
#define RESUME			0x7A
#define JEDECID			0x9F
#define POWERDOWN		0xB9
#define RELEASE			0xAB
#define READSFDP		0x5A
#define UNIQUEID		0x4B
#define FRAMSERNO		0xC3

//####################################################################//
//                         General size definitions					  //
//				B = Bytes; KiB = Kilo Bytes; MiB = Mega Bytes		  //					                    
//####################################################################//
#define B(x)		uint32_t(x*BYTE)
#define KB(x)		uint32_t(x*KiB)
#define MB(x)		uint32_t(x*MiB)

//####################################################################//
//					SFDP related defines 						  	  //
//####################################################################//
#define DWORD(x) x
#define FIRSTBYTE 0x01
#define SFDPSIGNATURE 0x50444653
#define ADDRESSOFSFDPDWORD(x,y) x+((y - 1) * 4)
#define ADDRESSOFSFDPBYTE(x,y,z) x+((y - 1) * 4)+(z - 1)
#define KB4ERASE_TYPE 0x0C
#define KB32ERASE_TYPE 0x0F
#define KB64ERASE_TYPE 0x10
#define KB256ERASE_TYPE 0x12
#define MS1 0b00000000
#define MS16 0b00000001
#define MS128 0b00000010
#define S1    0b00000011

//####################################################################//
//					Fixed SFDP addresses 						      //
//####################################################################//
#define SFDP_HEADER_ADDR 0x00
#define SFDP_SIGNATURE_DWORD 0x01
#define SFDP_NPH_DWORD 0x02
#define SFDP_NPH_BYTE 0x03
#define SFDP_PARAM_TABLE_LENGTH_DWORD 0x01
#define SFDP_PARAM_TABLE_LENGTH_BYTE 0x04
#define SFDP_BASIC_PARAM_TABLE_HDR_ADDR 0x08
#define SFDP_BASIC_PARAM_TABLE_NO 0x01
#define SFDP_MEMORY_DENSITY_DWORD 0x02
#define SFDP_SECTOR_MAP_PARAM_TABLE_NO 0x02
#define SFDP_ERASE1_BYTE 0x01
#define SFDP_ERASE1_INSTRUCTION_DWORD 0x08
#define SFDP_ERASE2_INSTRUCTION_DWORD 0x09
#define SFDP_SECTOR_ERASE_TIME_DWORD 0x0A
#define SFDP_CHIP_ERASE_TIME_DWORD 0x0B
#define SFDP_PROGRAM_TIME_DWORD 0x0B

//####################################################################//
//					Chip specific instructions 						  //
//####################################################################//

//######################### Winbond #########################//
#define WINBOND_MANID         0xEF
#define SPI_PAGESIZE          0x100
#define WINBOND_WRITE_DELAY   0x02
#define WINBOND_WREN_TIMEOUT  10L

//####################################################################//
//							Definitions 							  //
//####################################################################//
#define BUSY			0x01
#define STDSPI			0x0A
#define ALTSPI			0x0B
#define ENFASTREAD		0x01
#define WRTEN         	0x02
#define SUS           	0x80
#define WSE           	0x04
#define WSP           	0x08
#define ADS           	0x01            // Current Address mode in Status register 3
#define DUMMYBYTE     	0xEE
#define NULLBYTE      	0x00
#define NULLINT       	0x0000
#define NO_CONTINUE   	0x00
#define NOVERBOSE     	0x00
#define VERBOSE       	0x01
#define PASS          	0x01
#define FAIL          	0x00
#define NOOVERFLOW    	false
#define NOERRCHK      	false
#define PRINTOVERRIDE 	true
#define ERASEFUNC     	0xEF
#define BUSY_TIMEOUT  	1000000000L
#define arrayLen(x)   	(sizeof(x) / sizeof(*x))
#define lengthOf(x)   	(sizeof(x))/sizeof(byte)
#define BYTE          	1L
#define KiB           	1024L
#define MiB           	KiB * KiB
#define S             	1000L
#define TIME_TO_PROGRAM(x) (_byteFirstPrgmTime + (_byteAddnlPrgmTime * (x - 1)) )

//####################################################################//
//					    List of Supported data types 				  //
//####################################################################//
#define _BYTE_              0x01
#define _CHAR_              0x02
#define _WORD_              0x03
#define _SHORT_             0x04
#define _ULONG_             0x05
#define _LONG_              0x06
#define _FLOAT_             0x07
#define _STRING_            0x08
#define _BYTEARRAY_         0x09
#define _CHARARRAY_         0x0A
#define _STRUCT_            0x0B

//####################################################################//
//                              Macros                                //
//####################################################################//
#define _delay_us(us)   delayMicroseconds(us)
#define CHIP_SELECT		digitalWrite(_csPin, LOW);
#define CHIP_DESELECT	digitalWrite(_csPin, HIGH);

//############################################################################
// in Page,Sector and block read/write functions, can put 0 to read maximum bytes 
//############################################################################

//############################################################################
// W25qxx Class 
//############################################################################
class W25qxx {
	public:
        //#################################### Constructor ########################################//
        W25qxx(SPIClass *spi, uint8_t csPin);
        //################################ Initial / Chip Functions ###############################//
		bool begin(uint32_t flashChipSize = 0);
		
		uint32_t W25qxx_ReadID(void);
		//################################ Write / Read Byte ###############################//
		bool writeByte(uint32_t _addr, uint8_t data, bool errorCheck = true);
		uint8_t  readByte(uint32_t _addr, bool fastRead = false);
		//################################ Write / Read Byte Array ###############################//
		bool writeByteArray(uint32_t _addr, uint8_t *data_buffer, size_t bufferSize, bool errorCheck = true);
  		bool readByteArray(uint32_t _addr, uint8_t *data_buffer, size_t bufferSize, bool fastRead = false);
		//################################ Write / Read Page ###############################//  
		// bool writePage(uint8_t *data_buffer, uint32_t Page_Address, uint32_t)

		//################################ Erase functions ###############################//
		bool eraseSection(uint32_t _addr, uint32_t _sz);
		bool eraseSector(uint32_t _addr);
		bool eraseBlock32K(uint32_t _addr);
		bool eraseBlock64K(uint32_t _addr);
		bool eraseChip(void);
    private:
        //################################### Private Variables ###################################//
        // Hardware define
		uint8_t _csPin;
		SPIClass *_spi;

		bool pageOverflow;
		bool SPIBusState = false;
		bool chipPoweredDown = false;
		bool address4ByteEnabled = false;
		bool _loopedOver = false;
		uint8_t cs_mask, errorcode, stat1, stat2, stat3, _SPCR, _SPSR, _a0, _a1, _a2;
		// uint8_t errorCode, stat1, stat2, stat3;
		struct      chipID {
                bool supported;
                bool supportedMan;
                bool sfdpAvailable;
                uint8_t manufacturerID;
                uint8_t memoryTypeID;
                uint8_t capacityID;
                uint32_t capacity;
                uint32_t eraseTime;
              };
              chipID _chip;



    	//################################### Private Functions ###################################//
		uint8_t W25qxx_Spi(uint8_t Data);
		bool _notBusy(uint32_t timeout = BUSY_TIMEOUT);
		uint8_t _readStat1(void);
		uint8_t _readStat2(void);
		uint8_t _readStat3(void);
		bool _writeEnable(void);
		bool _writeDisable(void);
		bool _getJedecId(void);
		bool _prep(uint8_t opcode, uint32_t _addr, uint32_t size = 0);
		bool _isChipPoweredDown(void);
		bool _enable4ByteAddressing(void);


};

#endif
