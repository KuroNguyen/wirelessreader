#include <WiFi.h>
#include <HTTPClient.h>
#include <MFRC522.h>
#include <ArduinoJson.h>
#include "w25qxx.h" 
#include <string.h>

SPIClass * hspi = NULL;
W25qxx * w25;

const char* ssid = "CNS-Guest";
const char* password =  "sgi19!2021";

//const char* ssid = "C-316";
//const char* password =  "tumotdenchin";
//DynamicJsonDocument doc;
 
void setup() {
 
  Serial.begin(115200);
  delay(1000);

  // Init External flash memory
  hspi = new SPIClass(HSPI);
  hspi->beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
  hspi->begin();
  w25 = new W25qxx(hspi, 15);
  delay(2000);
  // Test JSON memory
  Serial.printf("Heap size %d\r\n", ESP.getHeapSize()); 

  
 
  WiFi.begin(ssid, password); 
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println("Connected to the WiFi network");
  Serial.println(ESP.getFreeHeap());
  
  /**
   * Test
   */
  Serial.println("Whole chip erase");
  uint32_t time1 = micros();
  w25->eraseChip();
  Serial.printf("Erased chip after %d\r\n",micros()-time1);

  time1 = micros();
  String s = "1111000000001090012";
//  numStr2Hex(s);
  Serial.printf("End convert after %d\r\n",micros()-time1);

}
 
const char* root_ca= \
"-----BEGIN CERTIFICATE-----\n" \
"MIIFWjCCBEKgAwIBAgISAzPyTsBVGz3oF9irg8jlqIJrMA0GCSqGSIb3DQEBCwUA\n" \
"MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD\n" \
"ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0xOTA1MjcwNTQwNTFaFw0x\n" \
"OTA4MjUwNTQwNTFaMBwxGjAYBgNVBAMTEXVhdC1hcGkudmluYWlkLnZuMIIBIjAN\n" \
"BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtsnKwpuroUO7qTPO+9gLgpbRQA3k\n" \
"wSPVkq3LhcpSL/KyMGdkipieOreQDTy8jsDKuGbjZ5dKiOwDRUkX0ZWMS110WLSm\n" \
"KZb5T9QixiZ5jEjmuU/FnuGN7s/l/UtrK3Bn1I/zcPEzqGuEhXFLr6t2/bG2jkZ5\n" \
"dl6EalGTHrK6XhChoalX3IqaQVfGNi+LJcoSyNFLucXMS+D2oPI+7NV126zIeoAM\n" \
"ZH2XMQdwwZ/RdHprr+0fQhDYyD1pwSXS8gpiHKpabmZZTgFDdl+FVFAdiCPiFna6\n" \
"7P8kpJoekYybQD8hxkWjulxjKupaVsatVFrIJlS/LKFuKU4RX8SaLtBV3wIDAQAB\n" \
"o4ICZjCCAmIwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggr\n" \
"BgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBSp+hadanoXyTd4e2K8CshS\n" \
"oqVfVjAfBgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBvBggrBgEFBQcB\n" \
"AQRjMGEwLgYIKwYBBQUHMAGGImh0dHA6Ly9vY3NwLmludC14My5sZXRzZW5jcnlw\n" \
"dC5vcmcwLwYIKwYBBQUHMAKGI2h0dHA6Ly9jZXJ0LmludC14My5sZXRzZW5jcnlw\n" \
"dC5vcmcvMBwGA1UdEQQVMBOCEXVhdC1hcGkudmluYWlkLnZuMEwGA1UdIARFMEMw\n" \
"CAYGZ4EMAQIBMDcGCysGAQQBgt8TAQEBMCgwJgYIKwYBBQUHAgEWGmh0dHA6Ly9j\n" \
"cHMubGV0c2VuY3J5cHQub3JnMIIBBAYKKwYBBAHWeQIEAgSB9QSB8gDwAHUAdH7a\n" \
"gzGtMxCRIZzOJU9CcMK//V5CIAjGNzV55hB7zFYAAAFq+AU01QAABAMARjBEAiB7\n" \
"jGOxOrYKmRVN5rSUcELjODI+SZyzI9csPGKxsn5a3AIgdGh+rhB9IwZsbMRbKzAn\n" \
"WfeSISHOVShNuPNUm7oaFc4AdwBj8tvN6DvMLM8LcoQnV2szpI1hd4+9daY4scdo\n" \
"VEvYjQAAAWr4BTbGAAAEAwBIMEYCIQDKH0e8CNPoOKX0sOLq8ucFwt8bIcZwhDXy\n" \
"s9ZhxTdcCQIhAIG2ag3ypFyo4s3jg00tSCTqQeaed6kR4JAgUu0hHtcQMA0GCSqG\n" \
"SIb3DQEBCwUAA4IBAQBcoRc88Fcm7VglRyZnlKqaqTjU8ntm+ft4ss+4zxwUJcGr\n" \
"eDeoOaKq5QEac4ksP59wWuA+2+NmWUnqRlYgMyaMbIXaFM/v6XYCOljYCiDnG/+L\n" \
"sxv3nG8jP10PdCVxVinFccPJ12jUX+q3LrxduHZc/6Hz2hAngaQZmwq9D0TuCj2J\n" \
"AGC3MLJ5kXuDBOiJPLWD/1d9hUeJUQ60y6EPrrgbxQ97IR09iyhhSvQKy9wzkeAO\n" \
"54ik6AGlqtzC0L9/B8hpZKgtlmneyWDzBt+EjcaA2OJK26GpoYs8g8Igku0n9ssr\n" \
"bNxeJ4F2IH4QdjhQuUXIo9Zilqltc7EGof+Mv70c\n" \
"-----END CERTIFICATE-----\n";

const String name1 = "Authorization";
const String value = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkAyIiwiZXhwIjoxNTY4ODY2ODkxLCJyb2xlcyI6WyJBRE1JTiIsIkNIRUNLRVIiXX0.uOWWQpXRmWIjXStGCbyRl4D-nl8LoAFuY_zfrKgXE7fVinHTyJhl4x-Qlq-6qwkZ1rhPqsw9O3Eg9UzgIeDlog";
 
void loop() {
 
  if ((WiFi.status() == WL_CONNECTED)) { //Check the current connection status

    uint32_t mTime = micros();
    HTTPClient http;
 
    http.begin("https://uat-api.vinaid.vn:1443/api/attendance/white-list/90100005?27", root_ca); //Specify the URL and certificate
      
    // Set token
//    http.setAuthorization(token);
    http.addHeader(name1,value,false,true);
    int httpCode = http.GET();                                                  //Make the request
 
    if (httpCode > 0) { //Check for the returning code
      Serial.printf("Time to request %d\r\n",micros()-mTime);

      String payload = http.getString();

      Serial.printf("Payload size %d\r\n",payload.length());

      w25->writeByte(0x00000004,0x04);
      Serial.println(w25->readByte(0x00000004)); 
      
      // Test JSON
        
//      size_t pos_begin = payload.lastIndexOf("\"data\":[") + 9;
      size_t pos_begin = payload.lastIndexOf("\"whiteList\":[") + 14;
      size_t pos_end   = payload.lastIndexOf("\"]}");
      Serial.println(pos_begin);
      Serial.println(pos_end);
      uint32_t numOfCard = (pos_end + 1 - (pos_begin -2))/22;
      Serial.printf("Number of cards is %d\r\n",numOfCard);
      uint8_t * heapArray =(uint8_t *) malloc(numOfCard*10*sizeof(uint8_t));
      uint8_t k[10];
      size_t count_1 = 0;
            w25->writeByte(0x00000005,0x05);
      Serial.println(w25->readByte(0x00000005)); 
      for (uint32_t i = 0; i < numOfCard; i++) {
//        cardNum2hexArray(payload.substring(pos_begin+i,pos_begin+i+19),k);
        cardNum2hexArray(payload.substring(pos_begin+i*(19+3),pos_begin+i*(19+3)+19),k);
        memcpy(heapArray+count_1,k,10);
        count_1 +=10;
      }
      Serial.println("Check");
      Serial.println(heapArray[0]);
      
      // write to flash
      w25->writeByteArray(0x000000010,heapArray,100*10);
      uint8_t * testArray =(uint8_t *) malloc(numOfCard*10*sizeof(uint8_t));

      // Searching test
      // Start addr
      uint32_t beginAddr = 0x00000010;
      uint32_t endAddr = beginAddr + (100*10);
      Serial.printf("Start Address is 0x%08x\r\n",beginAddr);
      Serial.printf("End Address is 0x%08x\r\n",endAddr);
      uint32_t startTime1 = micros();
      uint8_t num2find[] = {0x01,0x11,0x10,0x00,0x00,0x00,0x01,0x09,0x05,0x82};
      
      bool ans = binary_search_flash(beginAddr,endAddr,num2find,10,5);
      if (ans) {
        Serial.println(xPortGetCoreID());
        Serial.printf("Have answer in %d\r\n",micros() - startTime1);
      } else {
        Serial.printf("No answer in %d\r\n",micros() - startTime1);
      }
      
//      Serial.printf("No answer in %d\r\n",micros() - startTime1);
//      w25->readByteArray(0x00000010,num2find1,10);
//      int res = memcmp(num2find,num2find1,7);
//      if (res > 0) {
//        Serial.println("a");
//      } else if (res<0) {
//        Serial.println("b");
//      } else {
//        Serial.println("c");
//      }

      // Dump Card
//      for (uint32_t i = 0; i < numOfCard; i++) {
//        Serial.printf("Card %d is: ",i);
//        for (int p = 0;p<10;p++) {
//          Serial.printf("%02x",heapArray[i*10+p]);  
//        }
//        Serial.printf("\r\n");
//      }
      
      free(heapArray);
      Serial.printf("Free Heap size after array %d\r\n", ESP.getFreeHeap());
      Serial.printf("Time to request %d\r\n",micros()-mTime);
        
        
        
      }
 
    else {
      Serial.println("Error on HTTP request");
    }
 
    http.end(); //Free the resources
  }
 
  delay(2000);
  
}

void cardNum2hexArray (String cardNumber, uint8_t cardNum[]) {
  // Check length of card number
  if (!(cardNumber.length() == 19)) return;

  // Append "0" first
  cardNum[0] = (cardNumber[0]-48);
  size_t num = 1;
  for (int i = 1; i < 19; i=i+2) {
    cardNum[num] = ((cardNumber[i]-48) << 4) + (cardNumber[i+1]-48);
    num++;
  }
}

bool binary_search_flash(uint32_t startAddr, uint32_t endAddr, uint8_t *dataCheck, int dataStep, int checkNum) {
  // Check address
  if (endAddr < startAddr) {
    Serial.println("False input");
    return false;
  }
  // Check dataStep
  if ( ((endAddr - startAddr) % dataStep) != 0 ) {
    Serial.println("False input");
    return false; 
  }
  int unCheckNum = dataStep - checkNum;
//  uint32_t midAddr = (endAddr - startAddr)/2 + startAddr;
  uint8_t tempArray[checkNum];
  if (startAddr == endAddr) {
    // Load startAddr
    w25->readByteArray(startAddr + unCheckNum,tempArray,checkNum);
    // Compare
    int res = memcmp(dataCheck + unCheckNum,tempArray,checkNum);
    if (res != 0) {
      return false;
    } else return true;
  } else if ( startAddr == (endAddr - dataStep) ) {
    // Load startAddr
    w25->readByteArray(startAddr + unCheckNum,tempArray,checkNum);
    // Compare
    int res = memcmp(dataCheck + unCheckNum,tempArray,checkNum);
    if (res > 0) {
      return binary_search_flash(endAddr, endAddr, dataCheck, dataStep, checkNum);
    } else if (res < 0) {
      return false;
    } else return true;
  } else {
    // Calculate midAddr
    uint32_t midAddr;
    if ( ((endAddr - startAddr)/dataStep) % 2 == 0 ) {
      midAddr = (endAddr - startAddr)/2 + startAddr;
    } else {
      midAddr = (((endAddr - startAddr)/dataStep)/2)*dataStep + startAddr;
    }
//    uint32_t midAddr = (endAddr - startAddr)/2 + startAddr;
    // Load startAddr
    w25->readByteArray(midAddr + unCheckNum,tempArray,checkNum);
    
    // Print dataCheck and tempArray
    Serial.printf("Data check: ");
    for (int i = 0; i < dataStep; i++) {
      Serial.printf("%02x", dataCheck[i]);
    }
    Serial.printf("\r\n");

    Serial.printf("Data temp: ");
    for (int i = 0; i < checkNum; i++) {
      Serial.printf("%02x",tempArray[i]);
    }
    Serial.printf("\r\n");
       
    // Compare
    int res = memcmp(dataCheck + unCheckNum,tempArray,checkNum);
    if (res > 0) {
      Serial.println("Large");
      return binary_search_flash(midAddr, endAddr, dataCheck, dataStep, checkNum);
    } else if (res < 0) {
      Serial.println("Small");
      return binary_search_flash(startAddr, midAddr, dataCheck, dataStep, checkNum);
    } else return true;
  }
}
